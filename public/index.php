<?php

$loader = require_once '../vendor/autoload.php';

use App\App;
use App\Utils\DotEnv;

error_reporting(E_ALL);
ini_set('display_errors', true);
date_default_timezone_set('Europe/Paris');

App::setBasePath(dirname(__DIR__));
App::setLoader($loader);

DotEnv::load();

$app = new App();
$app->run();
