<?php

namespace App\Documents;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Odm;

/**
 * @Odm\Document
 * @Odm\HasLifecycleCallbacks
 */
class Document
{
  /**
   * @Odm\Id(strategy="UUID")
   */
  public $id;

  /**
   * Date at which the document was created
   * @Field(type="date")
   */
  public $createdAt;

  /**
   * Date at which the document was last updated
   * @Field(type="date")
   */
  public $updatedAt;

  /**
   * Will fill the createdAt field if null and replace updatedAt with a newer timestamp
   */
  public function fillTimestamps(): self
  {
    $dateTime = new DateTime();
    if ($this->createdAt == null) {
      $this->createdAt = $dateTime;
    }
    $this->updatedAt = $dateTime;

    return $this;
  }
}