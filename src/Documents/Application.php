<?php

namespace App\Documents;

use App\Utils\UUIDGenerator;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Odm;

/**
 * Application is a object use to authenticate call to the API with a token
 *
 * @Odm\HasLifecycleCallbacks()
 * @Odm\Document(collection="applications")
 */
class Application extends Document
{
  /**
   * @Odm\Id(strategy="UUID")
   */
  public $id;

  /**
   * Name of the application set by a user
   * @var string
   * @Odm\Field(type="string")
   */
  public $name;

  /**
   * Secret token used to authenticate
   * @var string
   * @Odm\Field(type="string")
   */
  public $token;

  /**
   * @Odm\PrePersist()
   */
  public function prePersist() {
    if ($this->token === null) {
      $this->token = UUIDGenerator::generate();
    }
  }
}