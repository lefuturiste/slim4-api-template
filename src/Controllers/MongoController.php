<?php

namespace App\Controllers;

use App\Documents\Application;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class MongoController extends Controller
{

  public function test(Request $request, Response $response)
  {
    $dm = $this->container->get(DocumentManager::class);

    // CREATE
    $document = new Application();
    $document->name = "Hello this is an app";
    $document->token = "this is a token";
    $document->createdAt = new \DateTime();
    $document->updatedAt = new \DateTime();

    $dm->persist($document);
    $dm->flush();

    // GET ALL
    //$docs = $dm->getRepository(Application::class)->findAll();

    // GET ONE, $doc will be null if the id is invalid or not found
    //$doc = $dm->getRepository(Application::class)->find("8807bb6b89a53fafa3f3776102786ab5");

    // UPDATE
    // $doc = $dm
    //   ->createQueryBuilder(Application::class)
    //   ->field('id')->equals("8807bb6b89a53fafa3f3776102786ab5")
    //   ->getQuery()
    //   ->getSingleResult();
    // $dm->flush();

    // REMOVE
    // $doc = $dm
    //   ->createQueryBuilder(Application::class)
    //   ->remove()
    //   ->field('id')->equals("8807bb6b89a53fafa3f3776102786ab5")
    //   ->getQuery()
    //   ->execute();
    // $dm->flush();

    return $this->withJSON(
      $response,
      [
        'success' => true,
        //'data' => $doc
      ]
    );
  }
}