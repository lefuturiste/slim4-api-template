<?php
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class PagesController extends Controller {

  public function getHome(Request $request, Response $response) {
    return $this->withJSON(
      $response, [
        'success' => true
      ]);
  }

  public function getPing(Request $request, Response $response) {
    return $this->withJSON($response, [
      'success' => true,
      'data' => 'pong'
    ]);
  }
}