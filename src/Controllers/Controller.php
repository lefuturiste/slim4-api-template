<?php
namespace App\Controllers;

use App\Session;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;

class Controller {

  protected ContainerInterface $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  public function withJSON(Response $response, $data = [], $code = 200): Response
  {
    $response->getBody()->write(json_encode($data));
    return $response
      ->withHeader('Content-Type', 'application/json')
      ->withStatus($code);
  }

  public function session()
  {
    return $this->container->get(Session::class);
  }
  
}
