<?php

use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use MongoDB\Client;
use Psr\Container\ContainerInterface;

return [
  DocumentManager::class => function (ContainerInterface $container) {
    // parse the uri (savage way)
    $uri = $container->get('db')['uri'];
    $uriComponents = explode('/', $uri);
    $dbName = end($uriComponents);
    $uriWithoutDbName = implode('/', array_slice($uriComponents, 0, count($uriComponents)-1));

    $config = new Configuration();
    $config->setProxyDir(__DIR__ . '/../Proxies');
    $config->setProxyNamespace('Proxies');
    $config->setHydratorDir(__DIR__ . '/../Hydrators');
    $config->setHydratorNamespace('Hydrators');
    $config->setDefaultDB($dbName);
    $config->setMetadataDriverImpl(AnnotationDriver::create(__DIR__ . '/../Documents'));
    
    spl_autoload_register($config->getProxyManagerConfiguration()->getProxyAutoloader());

    $client = new Client(
      $uriWithoutDbName,
      [],
      ['typeMap' => DocumentManager::CLIENT_TYPEMAP]
    );

    return DocumentManager::create($client, $config);
  }
];
