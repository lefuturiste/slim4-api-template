<?php
namespace App;

use App\Middlewares\JSONBodyParserMiddleware;
use App\Utils\ContainerBuilder;
use Slim\Factory\AppFactory;
use Throwable;

class App
{
  /**
   * The absolute fs path of the root of the application
   */
  private static string $basePath = '';

  private static $loader = null;

  private $app = null;

  public function __construct()
  {
    //$container = new Container();
    $container = ContainerBuilder::direct();
    AppFactory::setContainer($container);
    $app = AppFactory::create();

    // error handler
    $errorMiddleware = $app->addErrorMiddleware(true, true, true);
    $errorMiddleware->setDefaultErrorHandler(function ($_, Throwable $exception) use ($app) {
      $payload = [
        'success' => false,
        'errors' => [
          [
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $exception->getTraceAsString()
          ]
        ]
      ];

      $response = $app->getResponseFactory()->createResponse();
      $response->getBody()->write(
        json_encode($payload, JSON_UNESCAPED_UNICODE)
      );

      $code = 500;
      if ($exception->getCode() == 404 || $exception->getCode() == 405) {
        $code = $exception->getCode();
      }

      return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus($code);
    });

    $app->add(new JSONBodyParserMiddleware());
    $app->add(new Middlewares\CORSMiddleware());

    $app->get('/', Controllers\PagesController::class . ':getHome');
    $app->get('/ping', Controllers\PagesController::class . ':getPing');

    $app
      ->get('/mongodb-test', Controllers\MongoController::class . ':test');

    //$app->get('/health', [Controllers\HealthController::class, 'getHealth']);
    $this->app = $app;
  }

  // protected function configureContainer(\DI\ContainerBuilder $builder)
  // {
  //   ContainerBuilder::getContainerBuilder($builder);
  // }

  public static function setBasePath(string $basePath): void
  {
    self::$basePath = $basePath;
  }

  public static function getBasePath(): string
  {
    return self::$basePath;
  }

  public static function setLoader($loader)
  {
    self::$loader = $loader;
  }

  public static function getLoader()
  {
    return self::$loader;
  }

  public function run()
  {
    $this->app->run();
  }
}

