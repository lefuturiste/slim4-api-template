<?php

namespace App\Utils;

use App\App;

class DotEnv
{
  public static function load(?string $basePath = null): void
  {
    $dotenv = \Dotenv\Dotenv::createImmutable($basePath ?? App::getBasePath());
    $dotenv->load();
  }
}