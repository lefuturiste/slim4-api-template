<?php

namespace App\Utils;

use App\App;
use Exception;
use Psr\Container\ContainerInterface;

class ContainerBuilder
{
  private static array $definitions = [
    'app',
    'containers',
    'database'
  ];

  public static function getContainerBuilder(?string $basePath, \DI\ContainerBuilder $containerBuilder = null): \DI\ContainerBuilder
  {
    if ($containerBuilder == null){
      $containerBuilder = new \DI\ContainerBuilder();
    }
    foreach (self::$definitions as $def) {
      $containerBuilder->addDefinitions(($basePath ?? App::getBasePath()) . "/src/config/{$def}.php");
    }

    return $containerBuilder;
  }

  public static function getContainerFromBuilder(\DI\ContainerBuilder $containerBuilder): ?ContainerInterface
  {
    try {
      return $containerBuilder->build();
    } catch (Exception $e) {
      return null;
    }
  }

  public static function direct(?string $basePath = null): ContainerInterface
  {
    return self::getContainerFromBuilder(self::getContainerBuilder($basePath));
  }
}
