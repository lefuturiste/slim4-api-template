<?php

namespace App\Utils;

use Ramsey\Uuid\Uuid as UuidHelper;

class UUIDGenerator {
  public static function generate() {
    // for now using the experimental UUID 6 generation method
    return UuidHelper::uuid6()->toString();
  }
}