<?php

namespace App\Middlewares;

use App\DocumentHelper as DocHelper;
use App\Documents\Application;
use App\Session;
use Doctrine\ODM\MongoDB\MongoDBException;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface;

class AuthMiddleware {
  private ContainerInterface $container;

  private Session $session;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
    $this->session = $container->get(Session::class);
  }

  /**
   * @param ServerRequestInterface $request
   * @param $handler
   * @return mixed
   * @throws MongoDBException
   */
  public function __invoke(Request $request, $handler)
  {
    if ($request->hasHeader('Authorization')) {
      $token = str_replace('Bearer ', '', $request->getHeader('Authorization'))[0];

      // first try the admin token
      $authenticated = false;
      if ($token === $this->container->get('masterApiKey')) {
        $this->session->withAdmin(true);
        $authenticated = true;
      }

      // if none of the search was successful, we show a error
      if (!$authenticated) {
        $response = new Response();
        $response->getBody()->write(json_encode([
          'success' => false,
          'errors' => [['code' => 'invalid-auth', 'message' => 'Invalid Authorization header']]
        ]));
        return $response
          ->withHeader('Content-Type', 'application/json')
          ->withStatus(401);
      }
    }
    return $handler->handle($request);
  }
}