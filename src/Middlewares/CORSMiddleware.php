<?php
namespace App\Middlewares;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CORSMiddleware {
  public function __invoke(Request $request, $handler)
  {
    $response = $handler->handle($request);
    if ($request->getMethod() === 'OPTIONS') {
      $response = new Response();
      $response->getBody()->write(json_encode([
        'success' => true
      ]));
      $response = $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(200);
    }
    $response = $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS')
        ->withHeader('Access-Control-Allow-Headers', 'Origin, Content-Type, Authorization');
    return $response;
  }
}