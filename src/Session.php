<?php

namespace App;

use App\Documents\Application;
use Psr\Container\ContainerInterface;

class Session
{
  /**
   * This property will be non null when a app token is used
   * @var ?Application
   */
  private $application = null;

  /**
   * This property will be true when a master key is used
   * @var bool
   */
  private $admin = false;

  /**
   * @var ContainerInterface
   */
  private ContainerInterface $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  /**
   * @return boolean
   */
  public function isAdmin(): bool
  {
    return $this->admin;
  }

  public function isApplication(): bool
  {
    return $this->application !== null;
  }

  public function withAdmin(bool $val = true)
  {
    $this->admin = $val;
  }

  public function withApplication(Application $app)
  {
    $this->application = $app;
  }

  /**
   * @return Application|null
   */
  public function getApplication(): ?Application
  {
    return $this->application;
  }
}